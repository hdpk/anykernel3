#!/bin/bash

# Kill any existing adb and fastboot processes
pkill adb > /dev/null 2>&1
pkill fastboot > /dev/null 2>&1
adb kill-server > /dev/null 2>&1

# Start ADB server
adb start-server > /dev/null 2>&1

# Give some time for the adb server to start
sleep 1

check_device() {
    # Check if an ADB device is connected
    device=$(adb devices | grep -w "device")
    if [ -n "$device" ]; then
        echo "Device is connected, rebooting into bootloader..."
        adb reboot bootloader
        wait_bootloader
    else
        echo "No ADB device detected, checking fastboot mode..."
        check_fastboot
    fi
}

wait_bootloader() {
    # Wait for the device to enter bootloader mode
    echo "Waiting for device to enter bootloader mode..."
    sleep 2
    check_fastboot
}

check_fastboot() {
    # Wait for device to appear in fastboot mode
    fastboot devices | grep -w "fastboot" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "Device is in fastboot mode, proceeding with flashing..."
        flash_images
    else
        echo "No device found in ADB or Fastboot mode."
        device_error
    fi
}

flash_images() {
    # Flash the images
    echo "Flashing static partitions..."
    fastboot flash boot boot.img || flash_error
    fastboot flash vendor_kernel_boot vendor_kernel_boot.img || flash_error
    fastboot flash dtbo dtbo.img || flash_error

    echo "Success! Rebooting to fastbootd..."
    fastboot reboot-fastboot || flash_error
    sleep 5

    echo "Flashing dynamic partitions..."
    fastboot flash system_dlkm system_dlkm.img || flash_error
    fastboot flash vendor_dlkm vendor_dlkm.img || flash_error

    echo "Success! Rebooting to System..."
    fastboot reboot || flash_error
    end
}

flash_error() {
    echo "There was an error flashing the device."
    exit 1
}

device_error() {
    echo "There was an error finding your device."
    exit 1
}

end() {
    adb kill-server
    echo "Flashing process completed."
    exit 0
}

# Start the process by checking for connected devices
check_device
