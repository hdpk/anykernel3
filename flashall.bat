@echo off

REM Kill any existing adb and fastboot processes
taskkill /F /IM adb.exe /T >nul 2>&1
taskkill /F /IM fastboot.exe /T >nul 2>&1
adb kill-server >nul 2>&1

:start_adb
REM Start ADB server
adb start-server >nul 2>&1

REM Give some time for the adb server to start
timeout /t 1 /nobreak >nul

:check_device
REM Check if ADB device is connected
FOR /F "tokens=1,2" %%a IN ('adb devices') DO (
    IF "%%b"=="device" (
        SET "deviceFound=true"
        REM Device is connected, reboot into bootloader
        echo Device is connected, rebooting into bootloader...
        adb reboot bootloader
        goto wait_bootloader
    )
)

REM If no device is found, check fastboot directly
IF NOT DEFINED deviceFound (
    echo No ADB device detected, checking fastboot mode...
    goto check_fastboot
)

:wait_bootloader
REM Wait for the device to enter bootloader mode
echo Waiting for device to enter bootloader mode...
timeout /t 2 /nobreak >nul
goto check_fastboot_bootloader

:check_fastboot_bootloader
fastboot devices | findstr /C:"fastboot" >nul 2>&1
if %errorlevel%==0 (
    REM Device is in fastboot mode, proceed with flashing
    echo Device is in fastboot mode, proceeding with flashing...
    goto flash_images
) else (
    REM Device is not in fastboot mode yet, continue waiting
    timeout /t 2 /nobreak >nul
    goto check_fastboot_bootloader
)

:check_fastboot
REM Wait for device to appear in fastboot mode
fastboot devices | findstr /C:"fastboot" >nul 2>&1
if %errorlevel%==0 (
    REM Device is in fastboot mode, proceed with flashing
    echo Device is in fastboot mode, proceeding with flashing...
    goto flash_images
) else (
    REM No fastboot device found, error out
    echo No device found in ADB or Fastboot mode.
    goto device_error
)

:flash_images
REM Flash the images
echo Flashing static partitions...
fastboot flash boot boot.img || goto flash_error
fastboot flash vendor_kernel_boot vendor_kernel_boot.img || goto flash_error
fastboot flash dtbo dtbo.img || goto flash_error

echo Success! Rebooting to fastbootd...
fastboot reboot-fastboot || goto flash_error
timeout /t 5 /nobreak >nul

echo Flashing dynamic partitions...
fastboot flash system_dlkm system_dlkm.img || goto flash_error
fastboot flash vendor_dlkm vendor_dlkm.img || goto flash_error

echo Success! Rebooting to System...
fastboot reboot || goto flash_error
goto end

:flash_error
echo There was an error flashing the device.
exit /b 1

:device_error
echo There was an error finding your device.
exit /b 1

:end
adb kill-server
REM Exit
exit /b 0